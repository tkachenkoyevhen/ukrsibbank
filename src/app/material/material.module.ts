import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule, MatNativeDateModule, MatInputModule, MatButtonModule, MatTableModule } from '@angular/material';


@NgModule({
  declarations: [],
  imports: [ CommonModule, MatInputModule, MatDatepickerModule,  MatNativeDateModule, MatButtonModule, MatTableModule ],
  exports: [ MatInputModule, MatDatepickerModule, MatButtonModule, MatTableModule ]
})
export class MaterialModule { }
