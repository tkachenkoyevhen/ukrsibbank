import { Component, OnInit, ViewChild } from '@angular/core';
import { CurrencyService } from './currency.service';

import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Observable, forkJoin, from } from 'rxjs';
import { map, tap, filter, scan, retry, catchError } from 'rxjs/operators';

import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';

export interface Currency {
  exchangedate: string;
  usd: number;
  eur: number;
  gbp: number;
}

const day:number = 24*60*60*1000;

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css'],
  providers: [ DatePipe ]
})
export class CurrencyComponent implements OnInit {
  private today:string;
  private dataSource: any = [];
  private displayedColumns: string[] = ['exchangedate', 'usd', 'eur', 'gbp'];
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private currency: CurrencyService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    // current dates
    this.today = this.datePipe.transform(new Date(), 'yyyyMMdd');


    let usd = this.currency.getData('USD', this.today);
    let eur = this.currency.getData('EUR', this.today);
    let gbp = this.currency.getData('GBP', this.today);
    let result: Currency[] = [];

    forkJoin(usd, eur, gbp).subscribe(
      data => {
        result.push({
          exchangedate: this.today,
          usd: data[0][0].rate.toFixed(2),
          eur: data[1][0].rate.toFixed(2),
          gbp: data[2][0].rate.toFixed(2)
        });
        
        this.dataSource = new MatTableDataSource(Object.assign([], result));
      }
    );
    this.dataSource.sort = this.sort;
  }

  updateTbl() {
    let actualDate: string,
        usd: any,
        eur: any,
        gbp: any;
    let result: Currency[] = [];
    let diffDays = Math.floor(Math.abs((new Date(this.currency.start).getTime() - new Date(this.currency.end).getTime())/(day)));

    for(let i = 0; i <= diffDays; i++) {
      actualDate = this.datePipe.transform(new Date(this.currency.end).setDate(new Date(this.currency.start).getDate()+i), 'yyyyMMdd');
      usd = this.currency.getData('USD', actualDate);
      eur = this.currency.getData('EUR', actualDate);
      gbp = this.currency.getData('GBP', actualDate);

      console.log(actualDate);

      forkJoin(usd, eur, gbp).subscribe(
        data => {
          result.push({
            exchangedate: data[0][0].exchangedate,
            usd: data[0][0].rate.toFixed(2),
            eur: data[1][0].rate.toFixed(2),
            gbp: data[2][0].rate.toFixed(2)
          });
          this.dataSource = new MatTableDataSource( Object.assign([i], result) );
          this.dataSource.sort = this.sort;
        }
      );
    }
    //this.currC.dataSource = Object.assign([], result);
    //this.dataSource = result;
    console.log(this.dataSource);
    //console.log(this.dateStart.value);
    //console.log(this.dateEnd.value);
    //console.log(this.diffDays);
  }
}
