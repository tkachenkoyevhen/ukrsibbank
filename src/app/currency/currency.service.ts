import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Accept': 'application/json',
    'user-key': 'bac0b59444cce8eb4527f2aa7840e41e'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  public start: any;
  public end: any;

  constructor(private http: HttpClient) { }

  getData(curr, date): Observable<any> {
    return this.http.get(`https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=${curr}&date=${date}&json`/*, httpOptions*/);
  }

}