import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CurrencyService } from './currency/currency.service'

const day:number = 24*60*60*1000;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ukrsibbank';

  // current dates
  public dateStart = new FormControl(new Date());
  public dateEnd = new FormControl(new Date());

  // initialize default range of date
  minStart = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 5);
  maxStart = new Date();

  minEnd = new Date();
  maxEnd = new Date();

  private diffDays: number = 0;

  constructor(public curr: CurrencyService) {}

  changeDate(form: string, event: any) {
    this.diffDays = Math.floor(Math.abs((new Date(this.dateStart.value).getTime() - new Date(this.dateEnd.value).getTime())/(day)));

    this.curr.start = this.dateStart.value;
    this.curr.end = this.dateEnd.value;

    switch (form) {
      case 'start':
        this.minEnd = new Date(event.value);
        this.maxEnd = new Date(this.dateEnd.value);
        break;
      case 'end':
          this.minStart = new Date(event.value);
        this.minStart.setDate(this.minStart.getDate() - 5);
        if(this.diffDays >= 5) {
          this.maxStart.setDate(this.maxStart.getDate() + 5);
        } else {
          this.maxStart = new Date(this.dateEnd.value);
        }
        break;
    }
  }
}
